#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO user exporter.
#

"""COYO user exporter - Export JSON Thread"""

import logging
import os
import json

from PyQt5.QtCore import QRunnable, pyqtSlot

from gui.signals.WorkerSignals import WorkerSignals

class ExportJSONThread(QRunnable):
    """Export to JSON thread"""

    def __init__(self,
                 appconfig,
                 filename,
                 users):
        """Initializes the thread

        :param appconfig: The AppConfig
        :param filename: The file name
        """
        super().__init__()

        logging.debug('Initializing ExportJSONThread')

        self.appconfig = appconfig
        self.filename = filename
        self.users = users

        self.signals = WorkerSignals()
        if not self.filename.endswith('.json'):
            logging.debug('Adding suffix ".json" to filename "{}"'.format(self.filename))
            self.filename += '.json'

    @pyqtSlot()
    def run(self):
        """Runs the thread"""
        try:
            logging.info('Exporting users to "{}"...'.format(self.filename))
            os.makedirs(os.path.dirname(self.filename), exist_ok=True)
            with open(self.filename, 'w', encoding='utf-8') as outfile_json:
                json.dump(self.users, outfile_json, ensure_ascii=False, skipkeys=True)
        except Exception as ex:
            logging.exception('Failed to write JSON file')
            self.signals.error.emit(ex)
        else:
            self.signals.result.emit(self.filename)
        finally:
            self.signals.finished.emit()
    