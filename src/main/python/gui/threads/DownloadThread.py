#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO user exporter.
#

"""COYO user exporter - Thread"""

import logging
import os
import json
import csv

from PyQt5.QtCore import QObject, QRunnable, pyqtSlot

from lib.coyo.CoyoUserStatus import CoyoUserStatus
from gui.signals.WorkerSignals import WorkerSignals

class DownloadThread(QRunnable):
    """Download thread"""

    def __init__(self,
                 ops,
                 userstatus=CoyoUserStatus.ALL):
        """Initializes the thread

        :param ops: The operations
        :param userstatus: The CoyoUserStatus
        """
        super().__init__()

        logging.debug('Initializing DownloadThread')

        self.ops = ops
        self.userstatus = userstatus

        self.signals = WorkerSignals()

    @pyqtSlot()
    def run(self):
        """Runs the thread"""
        try:
            users = []
            req_getallusers = self.ops.get_all_users(userstatus=self.userstatus)
            if req_getallusers[0]:
                users = req_getallusers[1]
                logging.info('Downloaded {} users.'.format(len(users)))
            else:
                raise Exception('Could not download all users.')
        except Exception as ex:
            logging.exception('Failed to download users')
            self.signals.error.emit(ex)
        else:
            self.signals.result.emit(users)
        finally:
            self.signals.finished.emit()
