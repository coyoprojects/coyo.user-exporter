#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO user exporter.
#

"""COYO user exporter - Export JSON Thread"""

import logging
import os
import json
import csv

from PyQt5.QtCore import QRunnable, pyqtSlot

from gui.signals.WorkerSignals import WorkerSignals

class ExportCSVThread(QRunnable):
    """Export to CSV thread"""

    def __init__(self,
                 appconfig,
                 filename,
                 users):
        """Initializes the thread

        :param appconfig: The AppConfig
        :param filename: The file name
        """
        super().__init__()

        logging.debug('Initializing ExportCSVThread')

        self.appconfig = appconfig
        self.filename = filename
        self.users = users

        self.signals = WorkerSignals()
        if not self.filename.endswith('.csv'):
            logging.debug('Adding suffix ".csv" to filename "{}"'.format(self.filename))
            self.filename += '.csv'

    def _get_fieldnames(self):
        """Gathers all field names

        :return array: The field names
        """
        fieldnames = []
        for user in self.users:
            for fieldname in user.keys():
                if not fieldname in fieldnames:
                    fieldnames.append(fieldname)
        fieldnames.append('group_names')
        fieldnames.append('role_names')
        return fieldnames

    def _extract_group_names(self, user):
        """Extracts the group names of the groups array

        :param user: The user containing the data
        :return array: The array containing the extracted group names
        """
        extracted_groups = []
        if 'groups' in user:
            for group in user['groups']:
                if 'displayName' in group:
                    extracted_groups.append(group['displayName'])
        return extracted_groups

    def _extract_role_names(self, user):
        """Extracts the role names of the roles array

        :param user: The user containing the data
        :return array: The array containing the extracted role names
        """
        extracted_roles = []
        if 'roles' in user:
            for role in user['roles']:
                if 'displayName' in role:
                    extracted_roles.append(role['displayName'])
        return extracted_roles

    def _sanitize_user(self, user, fields):
        """Prepares a user for the CSV output

        :param user: The user to clean
        :param fields: The fields to be printed
        :return object: The sanitized user
        """
        for field in fields:
            if not field in user:
                user[field] = ''
                user[field] = ''
        user['group_names'] = self._extract_group_names(user)
        user['role_names'] = self._extract_role_names(user)
        return user

    @pyqtSlot()
    def run(self):
        """Runs the thread"""
        try:
            logging.info('Exporting users to "{}"...'.format(self.filename))
            os.makedirs(os.path.dirname(self.filename), exist_ok=True)
            with open(self.filename, 'w', encoding='utf-8') as outfile_csv:
                fields = self._get_fieldnames()
                writer = csv.DictWriter(outfile_csv, fieldnames=fields, delimiter=self.appconfig.csv_delimiter)
                writer.writeheader()
                write_failed = []
                for user in self.users:
                    try:
                        writer.writerow(self._sanitize_user(user, fields))
                    except ValueError as vex:
                        logging.exception('Failed to write {}'.format(user))
                        val = {
                            'user': user['id'],
                            'error': str(vex)
                        }
                        write_failed.append(val)
                if len(write_failed) > 0:
                    logging.error('Failed to write {} {} to CSV file.'
                        .format(len(write_failed), 'user' if len(write_failed) == 1 else 'users'))
                    with open(self.filename + '.failed.json', 'w', encoding='utf-8') as outfile_json:
                        try:
                            json.dump(write_failed, outfile_json, ensure_ascii=False, skipkeys=True)
                        except Exception:
                            logging.exception('Failed to dump JSON file')
        except Exception as ex:
            logging.exception('Failed to write CSV file')
            self.signals.error.emit(ex)
        else:
            self.signals.result.emit(self.filename)
        finally:
            self.signals.finished.emit()
