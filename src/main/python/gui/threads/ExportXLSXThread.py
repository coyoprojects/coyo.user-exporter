#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO user exporter.
#

"""COYO user exporter - Export XLSX Thread"""

import logging
import os
import json
import datetime

import pandas as pd
from PyQt5.QtCore import QRunnable, pyqtSlot

from gui.signals.WorkerSignals import WorkerSignals

class ExportXLSXThread(QRunnable):
    """Export to XLSX thread"""

    def __init__(self,
                 appconfig,
                 filename,
                 users,
                 sheetname='Users'):
        """Initializes the thread

        :param appconfig: The AppConfig
        :param filename: The file name
        """
        super().__init__()

        logging.debug('Initializing ExportXLSXThread')

        self.appconfig = appconfig
        self.filename = filename
        self.users = users
        self.sheetname = sheetname

        self.signals = WorkerSignals()
        if not self.filename.endswith('.xlsx'):
            logging.debug('Adding suffix ".xlsx" to filename "{}"'.format(self.filename))
            self.filename += '.xlsx'

    def _get_fieldnames(self):
        """Gathers all field names

        :return array: The field names
        """
        fieldnames = []
        for user in self.users:
            for fieldname in user.keys():
                if not fieldname in fieldnames:
                    fieldnames.append(fieldname)
        fieldnames.append('group_names')
        fieldnames.append('role_names')
        return fieldnames

    def _extract_group_names(self, user):
        """Extracts the group names of the groups array

        :param user: The user containing the data
        :return array: The array containing the extracted group names
        """
        extracted_groups = []
        if 'groups' in user:
            for group in user['groups']:
                if 'displayName' in group:
                    extracted_groups.append(group['displayName'])
        return extracted_groups

    def _extract_role_names(self, user):
        """Extracts the role names of the roles array

        :param user: The user containing the data
        :return array: The array containing the extracted role names
        """
        extracted_roles = []
        if 'roles' in user:
            for role in user['roles']:
                if 'displayName' in role:
                    extracted_roles.append(role['displayName'])
        return extracted_roles

    def _sanitize_user(self, user, fields):
        """Prepares a user for the XLSX output

        :param user: The user to clean
        :param fields: The fields to be printed
        :return object: The sanitized user
        """
        for field in fields:
            if not field in user:
                user[field] = ''
            elif field == 'lastLogin':
                user[field] = datetime.datetime.fromtimestamp(int(user[field]) / 1000).strftime('%Y-%m-%d %H:%M:%S')
        user['group_names'] = self._extract_group_names(user)
        user['role_names'] = self._extract_role_names(user)
        return user

    def _get_sanitized_users(self, fields):
        """Prepares all users user for the XLSX output

        :param fields: The fields to be printed
        :return array: Array of users
        """
        new_users = []
        for user in self.users:
            new_users.append(self._sanitize_user(user, fields))
        return new_users

    @pyqtSlot()
    def run(self):
        """Runs the thread"""
        try:
            logging.info('Exporting users to "{}"...'.format(self.filename))
            os.makedirs(os.path.dirname(self.filename), exist_ok=True)
            fieldnames = self._get_fieldnames()
            df = pd.DataFrame(self._get_sanitized_users(fieldnames))
            with pd.ExcelWriter(self.filename, engine='xlsxwriter') as writer:
                df.to_excel(writer, sheet_name=self.sheetname)
                writer.save()
        except Exception as ex:
            logging.exception('Failed to write XLSX file')
            self.signals.error.emit(ex)
        else:
            self.signals.result.emit(self.filename)
        finally:
            self.signals.finished.emit()
