#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO user exporter.
#

"""COYO user exporter - Ops Signals"""

from PyQt5.QtCore import QObject, pyqtSignal

class OpsSignals(QObject):
    """
    Defines the signals available from operations.

    Supported signals are:

    fetchedusers
        `int` number of users fetched
        `int` number of all users in the system
    """
    fetchedusers = pyqtSignal(int, int)
