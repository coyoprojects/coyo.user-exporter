#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO user exporter.
#

"""COYO user exporter - Main widget"""

import logging
import os
import json
import csv

from PyQt5.QtCore import QThreadPool
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QWidget, QMessageBox, QDesktopWidget, QGridLayout, QLabel, QLineEdit, QPushButton, QFileDialog, QCheckBox, QComboBox

from lib.coyo.CoyoOps import CoyoOps
from lib.coyo.CoyoData import CoyoData
from lib.coyo.CoyoUserStatus import CoyoUserStatus
from gui.threads.DownloadThread import DownloadThread
from gui.threads.ExportJSONThread import ExportJSONThread
from gui.threads.ExportCSVThread import ExportCSVThread
from gui.threads.ExportXLSXThread import ExportXLSXThread

class MainWidget(QWidget):
    """Main widget GUI"""

    def __init__(self, appconfig, log, coyodata):
        """Initializes the main widget

        :param appconfig: The AppConfig
        :param log: The (end user) message log
        :param coyodata: The CoyoData
        """
        super().__init__()

        logging.debug('Initializing MainWidget')

        self.appconfig = appconfig
        self.log = log
        self.components = []
        self.coyodata = coyodata
        
        self.font_label_header = QFont()
        self.font_label_header.setBold(True)

        self.threadpool = QThreadPool()
        logging.debug('Multithreading with maximum {} threads.'.format(self.threadpool.maxThreadCount()))

        self.users = None

    def _checkbox_multibackend_statechanged(self):
        ischecked = self.checkbox_multibackend.isChecked()
        self.edit_sessionname.setEnabled(True if ischecked else False)

    def init_ui(self):
        """Initiates application UI"""
        logging.debug('Initializing MainWidget GUI')

        self.grid = QGridLayout()
        self.grid.setSpacing(10)

        self.label_empty = QLabel('')
        self.label_details_server = QLabel('Server details')
        self.label_details_user = QLabel('User details')
        self.label_details_actions1 = QLabel('Actions')
        self.label_details_actions2 = QLabel('Post-processing actions')
        self.label_baseurl = QLabel('Base URL')
        self.label_username = QLabel('Username')
        self.label_password = QLabel('Password')
        self.label_clientid = QLabel('Client ID')
        self.label_clientsecret = QLabel('Client Secret')
        self.label_multibackend = QLabel('Multi-backend')
        self.label_sessionname = QLabel('Session name')
        self.label_userstatus = QLabel('User status')

        self.label_details_server.setFont(self.font_label_header)
        self.label_details_user.setFont(self.font_label_header)
        self.label_details_actions1.setFont(self.font_label_header)
        self.label_details_actions2.setFont(self.font_label_header)

        self.edit_baseurl = QLineEdit()
        self.edit_baseurl.setText(self.coyodata.baseurl)
        self.edit_username = QLineEdit()
        self.edit_username.setText(self.coyodata.username)
        self.edit_password = QLineEdit()
        self.edit_password.setText(self.coyodata.password)
        self.edit_clientid = QLineEdit()
        self.edit_clientid.setText(self.coyodata.clientid)
        self.edit_clientsecret = QLineEdit()
        self.edit_clientsecret.setText(self.coyodata.clientsecret)

        self.checkbox_multibackend = QCheckBox()
        self.checkbox_multibackend.setChecked(self.coyodata.multibackend)
        self.checkbox_multibackend.stateChanged.connect(self._checkbox_multibackend_statechanged)
        self.edit_sessionname = QLineEdit()
        self.edit_sessionname.setText(self.coyodata.sessionname)

        self.combobox_userstatus = QComboBox()
        self.combobox_userstatus.addItems([status.name for status in CoyoUserStatus])

        self.button_start = QPushButton('Start user download')
        self.button_start.clicked[bool].connect(self._start_preparing_processing)
        self.button_save_json = QPushButton('Save as JSON')
        self.button_save_json.clicked[bool].connect(self._save_json)
        self.button_save_csv = QPushButton('Save as CSV')
        self.button_save_csv.clicked[bool].connect(self._save_csv)
        self.button_save_xlsx = QPushButton('Save as XLSX')
        self.button_save_xlsx.clicked[bool].connect(self._save_xlsx)

        self.components.append(self.edit_baseurl)
        self.components.append(self.edit_username)
        self.components.append(self.edit_password)
        self.components.append(self.edit_clientid)
        self.components.append(self.edit_clientsecret)
        self.components.append(self.checkbox_multibackend)
        self.components.append(self.edit_sessionname)
        self.components.append(self.combobox_userstatus)
        self.components.append(self.button_start)
        self.components.append(self.button_save_json)
        self.components.append(self.button_save_csv)
        self.components.append(self.button_save_xlsx)

        curr_gridid = 1

        self.grid.addWidget(self.label_details_server, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_baseurl, curr_gridid, 0)
        self.grid.addWidget(self.edit_baseurl, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_username, curr_gridid, 0)
        self.grid.addWidget(self.edit_username, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_password, curr_gridid, 0)
        self.grid.addWidget(self.edit_password, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_clientid, curr_gridid, 0)
        self.grid.addWidget(self.edit_clientid, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_clientsecret, curr_gridid, 0)
        self.grid.addWidget(self.edit_clientsecret, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_multibackend, curr_gridid, 0)
        self.grid.addWidget(self.checkbox_multibackend, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_sessionname, curr_gridid, 0)
        self.grid.addWidget(self.edit_sessionname, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_empty, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_details_user, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_userstatus, curr_gridid, 0)
        self.grid.addWidget(self.combobox_userstatus, curr_gridid, 1)

        curr_gridid += 1
        self.grid.addWidget(self.label_empty, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_details_actions1, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.button_start, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_empty, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.label_details_actions2, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.button_save_json, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.button_save_csv, curr_gridid, 0, 1, 3)

        curr_gridid += 1
        self.grid.addWidget(self.button_save_xlsx, curr_gridid, 0, 1, 3)

        self.setLayout(self.grid)
        self._reset_enabled()

    def _reset_enabled(self):
        """Resets all component to initial state"""
        logging.debug('Resetting components to enabled state')

        self._enable()
        self.button_save_json.setEnabled(False)
        self.button_save_csv.setEnabled(False)
        self.button_save_xlsx.setEnabled(False)
        self._checkbox_multibackend_statechanged()
        self.users = []

    def _disable(self):
        """Resets all component to disabled state"""
        logging.debug('Disabling components')

        for comp in self.components:
            comp.setEnabled(False)

    def _enable(self):
        """Resets all component to enabled state"""
        logging.debug('Enabling components')

        for comp in self.components:
            comp.setEnabled(True)

        self._checkbox_multibackend_statechanged()

    def show_dialog_critical(self, window_title, text, inf_text, detail_text):
        """Shows a dialog, critical

        :param window_title: The window title
        :param text: The text
        :param inf_text: The information text
        :param detail_text: The detail text
        """
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Critical)
        msg.setStandardButtons(QMessageBox.Ok)

        msg.setWindowTitle(window_title)
        msg.setText(text)
        msg.setInformativeText(inf_text)
        msg.setDetailedText(detail_text)
        
        return msg.exec_()

    def _callback_processing_result(self, users):
        """The processing callback, on result

        :param users: The users
        """
        logging.debug('Callback: Processing result')

        self.log('Downloaded {} users.'.format(len(users)))
        self.users = users
        self._enable()

    def _callback_processing_error(self, ex):
        """The processing callback, on error
        
        :param ex: The exception
        """
        logging.debug('Callback: Processing error')

        logging.error('Failed to download users: "{}"'.format(ex))
        self.log('Failed to download users.')
        self._reset_enabled()
        self.show_dialog_critical('Error', 'Download failed', 'Could not download all users: "{}"'.format(ex), None)

    def _callback_processing_finished(self):
        """The processing callback, on finished"""
        logging.debug('Callback: Processing finished')

    def _fetchedusers(self, nrofusers, nrofallusers):
        """Callback for users fetched
        
        :param nrofusers: Number of fetched users
        :param nrofallusers: Number of all users in the system
        """
        strtolog = 'Fetching users - 0%...'
        if nrofallusers == 0:
            strtolog = 'Fetched {}/{} users - {}%...'.format(nrofusers, nrofallusers, 0)
        if nrofallusers > 0:
            strtolog = 'Fetched {}/{} users - {:3.2f}%...'.format(nrofusers, nrofallusers, float(nrofusers / nrofallusers * 100))
        logging.info(strtolog)
        self.log(strtolog)

    def _start_processing(self):
        """Starts the processing"""
        logging.debug('Starting processing')

        try:
            ops = CoyoOps(self.appconfig, self.coyodata)
            ops.signals.fetchedusers.connect(self._fetchedusers)
            ops.init()
            if ops.valid:
                logging.info('Downloading all users...')
                self.log('Downloading all users...')
                ctext = self.combobox_userstatus.currentText()
                try:
                    userstatus = CoyoUserStatus[ctext]
                except Exception:
                    userstatus = CoyoUserStatus.ALL
                    logging.error('Could not parse user status "{}", falling back to default status "{}"'
                        .format(ctext, userstatus.name))
                thread = DownloadThread(ops, userstatus=userstatus)
                thread.signals.result.connect(self._callback_processing_result)
                thread.signals.error.connect(self._callback_processing_error)
                thread.signals.finished.connect(self._callback_processing_finished)
                self.threadpool.start(thread)
            else:
                logging.error('Could not retrieve access token.')
                self.log('Could not retrieve access token.')
                self._reset_enabled()
                self.show_dialog_critical('Error', 'Access token', 'Could not retrieve access token.', None)
        except Exception as ex:
            logging.exception('Could not start downloading: "{}"'.format(ex))
            self.log('Could not start downloading.')
            self.show_dialog_critical('Error', 'Error', 'Could not start downloading: "{}"'.format(ex), None)
            self._reset_enabled()

    def _start_preparing_processing(self):
        """Starts the processing process"""
        logging.debug('Starting preparing processing')

        try:
            self._disable()
            errors = []
            baseurl = self.edit_baseurl.text()
            if not baseurl:
                logging.error('Invalid base URL')
                errors.append('Please enter a base URL')
            username = self.edit_username.text()
            if not username:
                logging.error('Invalid username')
                errors.append('Please enter a username')
            password = self.edit_password.text()
            if not password:
                logging.error('Invalid password')
                errors.append('Please enter a password')
            clientid = self.edit_clientid.text()
            if not clientid:
                logging.error('Invalid client ID')
                errors.append('Please enter a client ID')
            clientsecret = self.edit_clientsecret.text()
            if not clientsecret:
                logging.error('Invalid client secret')
                errors.append('Please enter a client secret')
            if self.checkbox_multibackend.isChecked() and not self.edit_sessionname.text():
                logging.error('Invalid session name')
                errors.append('Please enter a valid session name if multi-backend mode is selected')
            if errors:
                err_str = ''
                for i, err in enumerate(errors):
                    if i > 0:
                        err_str += '\n'
                    err_str += '- ' + err
                self.show_dialog_critical('Error', 'Parameters missing', 'Some basic parameters are missing.', err_str)
                self._reset_enabled()
            else:
                self.coyodata = CoyoData(baseurl=baseurl,
                                         username=username,
                                         password=password,
                                         clientid=clientid,
                                         clientsecret=clientsecret,
                                         multibackend=self.checkbox_multibackend.isChecked(),
                                         sessionname=self.edit_sessionname.text())
                self._start_processing()
        except Exception as ex:
            logging.exception('Could not start downloading: "{}"'.format(ex))
            self.log('Could not start downloading.')
            self.show_dialog_critical('Error', 'Error', 'Could not start downloading: "{}"'.format(ex), err_str)
            self._reset_enabled()

    def _callback_export_result(self, filename):
        """The export callback, on result

        :param filename: The filename
        """
        logging.debug('Callback: Export result')

        logging.info('Successfully exported users to file "{}".'.format(filename))
        self.log('Successfully exported users to file "{}".'.format(filename))
        self._enable()

    def _callback_export_error(self, ex):
        """The export callback, on error
        
        :param ex: The exception
        """
        logging.debug('Callback: Export error')

        logging.error('Failed to export users to file: "{}"'.format(ex))
        self.log('Failed to export users to file.')
        self.show_dialog_critical('Error', 'Error', 'Failed to export users to file: "{}"'.format(ex), None)
        self._enable()

    def _callback_export_finished(self):
        """The export callback, on finished"""
        logging.debug('Callback: Export finished')

    def _save_json(self):
        """Starts the save JSON process"""
        logging.debug('Starting saving to JSON file')

        if self.users:
            try:
                self._disable()
                options = QFileDialog.Options()
                options |= QFileDialog.DontUseNativeDialog
                filename, _ = QFileDialog.getSaveFileName(self,
                                                          'Save as JSON file', '', 'JSON Files (*.json);;All Files (*)',
                                                          options=options)
                if filename:
                    logging.debug('Exporting users to "{}"...'.format(filename))
                    self.log('Exporting users to "{}"...'.format(filename))
                    thread = ExportJSONThread(self.appconfig, filename, self.users)
                    thread.signals.result.connect(self._callback_export_result)
                    thread.signals.error.connect(self._callback_export_error)
                    thread.signals.finished.connect(self._callback_export_finished)
                    self.threadpool.start(thread)
                else:
                    logging.debug('No file name given. Aborting...')

                    self._enable()
            except Exception:
                logging.exception('Could not write JSON to file')
                self.log('Could not write JSON to file.')
                self._enable()

    def _save_csv(self):
        """Starts the save CSV process"""
        if self.users:
            try:
                self._disable()
                options = QFileDialog.Options()
                options |= QFileDialog.DontUseNativeDialog
                filename, _ = QFileDialog.getSaveFileName(self,
                                                          'Save as CSV file', '', 'CSV Files (*.csv);;All Files (*)',
                                                           options=options)
                if filename:
                    logging.debug('Exporting users to "{}"...'.format(filename))
                    self.log('Exporting users to "{}"...'.format(filename))
                    thread = ExportCSVThread(self.appconfig, filename, self.users)
                    thread.signals.result.connect(self._callback_export_result)
                    thread.signals.error.connect(self._callback_export_error)
                    thread.signals.finished.connect(self._callback_export_finished)
                    self.threadpool.start(thread)
                else:
                    logging.debug('No file name given. Aborting...')

                    self._enable()
            except Exception:
                logging.exception('Could not write CSV to file: {}')
                self.log('Could not write CSV to file.')
                self._enable()

    def _save_xlsx(self):
        """Starts the save XLSX process"""
        if self.users:
            try:
                self._disable()
                options = QFileDialog.Options()
                options |= QFileDialog.DontUseNativeDialog
                filename, _ = QFileDialog.getSaveFileName(self,
                                                          'Save as XLSX file', '', 'XLSX Files (*.xlsx);;All Files (*)',
                                                           options=options)
                if filename:
                    logging.debug('Exporting users to "{}"...'.format(filename))
                    self.log('Exporting users to "{}"...'.format(filename))
                    thread = ExportXLSXThread(self.appconfig, filename, self.users)
                    thread.signals.result.connect(self._callback_export_result)
                    thread.signals.error.connect(self._callback_export_error)
                    thread.signals.finished.connect(self._callback_export_finished)
                    self.threadpool.start(thread)
                else:
                    logging.debug('No file name given. Aborting...')

                    self._enable()
            except Exception:
                logging.exception('Could not write XLSX to file')
                self.log('Could not write XLSX to file.')
                self._enable()
