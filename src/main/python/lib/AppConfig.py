#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO user exporter.
#

"""CoyoData - AppConfig"""

class AppConfig:
    """The application configuration"""

    def __init__(self,
                 csv_delimiter=','):
        """Initialization

        :param csv_delimiter: The CSV delimiter
        """
        self.csv_delimiter = csv_delimiter

        self.author = 'Denis Meyer'
        self.version = '1.0.0'
        self.copyright = '© 2018 Denis Meyer'
        self.img_logo_app = None
