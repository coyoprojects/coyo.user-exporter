#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO user exporter.
#

"""CoyoData - COYO data and credentials"""

class CoyoData:
    """Stores COYO data and and API credentials"""

    def __init__(self,
                 baseurl='',
                 username='',
                 password='',
                 clientid='',
                 clientsecret='',
                 multibackend=False,
                 sessionname=''):
        """Initialization
        
        :param baseurl: COYO base URL, e.g. "https://your-instance.coyocloud.com"
        :param username: COYO username, e.g. "ian.bold@coyo4.com"
        :param password: COYO password, e.g. "demo"
        :param clientid: COYO clientid, a name given in the COYO administration under "API Clients": /admin/api-clients
        :param clientsecret: COYO clientsecret, a UUID automatically generated for the clientid in the COYO administration under "API Clients": /admin/api-clients
        :param multibackend: Boolean value whether the COYO runs on multiple backends
        :param sessionname: A string defining the name of the 'Set-Cookie' header value. Only needed if multibackend is True
        """
        self.baseurl = baseurl if not baseurl.endswith('/') else baseurl[:-1]
        self.username = username
        self.password = password
        self.clientid = clientid
        self.clientsecret = clientsecret
        self.multibackend = multibackend
        self.sessionname = sessionname
