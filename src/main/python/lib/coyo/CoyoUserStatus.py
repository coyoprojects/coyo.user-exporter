from enum import Enum

class CoyoUserStatus(Enum):
    ALL = 'ALL'
    ACTIVE = 'ACTIVE'
    INACTIVE = 'INACTIVE'
    HIDDEN = 'HIDDEN'
    DELETED = 'DELETED'

def userstatus_str(userstatus):
    """Returns the string representation of the user status

    :param userstatus: The CoyoUserStatus
    """
    if userstatus == CoyoUserStatus.ALL:
        return ''
    return userstatus.name
