#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
# Copyright 2018 Denis Meyer
#
# This file is part of the COYO user exporter.
#

"""CoyoOps - COYO Operations"""

import logging
import json
import base64
import re
from urllib.parse import urlencode
from urllib.request import Request, urlopen
from urllib.error import HTTPError

from lib.coyo.CoyoUserStatus import CoyoUserStatus, userstatus_str
from gui.signals.OpsSignals import OpsSignals

class CoyoOps:
    """Authenticates with COYO and performs requests"""

    _urls = {
        'token': '/api/oauth/token',
        'users': '/api/users'
    }
    _urlparams = {
        'granttype': 'grant_type=password',
        'acctoken': 'access_token=',
        'orderby': '_orderBy=',
        'page': '_page=',
        'pagesize': '_pageSize=',
        'status': 'status=',
        'withadminfields': 'with=adminFields'
    }
    _urlpaths = {
        'translations': '/translations'
    }

    def __init__(self,
                 appconfig,
                 coyodata):
        """Initialization

        :param appconfig: The AppConfig
        :param coyodata: The CoyoData
        """
        self.appconfig = appconfig
        self.coyodata = coyodata

        self.signals = OpsSignals()

        self.additionalheaders = {}
        self.valid = False
        self.accesstoken = ''
        self.oauthdata = {}

    def init(self):
        """Retrieves the access token"""
        self._get_accesstoken()

    def get_all_users(self,
                      userstatus=CoyoUserStatus.ALL,
                      pagesize=50,
                      max_request_retries=3):
        """Retrieves all users

        :param userstatus: The CoyoUserStatus
        :param pagesize: The page size
        :param max_request_retries: Max request retries on error
        """
        ended = False
        page = 0
        orderby = 'lastname.sort,firstname.sort'
        widthadminfields = True
        users = []
        nrofallusers = -1
        self.signals.fetchedusers.emit(len(users), nrofallusers)
        retry_cnt = 0
        parsed_userstatus = userstatus_str(userstatus)
        logging.debug('Setting user status filter to "{}"'.format(parsed_userstatus))
        while not ended:
            logging.info('{} users loaded. Getting the next {} users of page {}'
                .format(len(users), pagesize, page))
            req_getusers = self.get_users(userstatus=parsed_userstatus,
                                          page=page,
                                          pagesize=pagesize,
                                          orderby=orderby,
                                          widthadminfields=widthadminfields)
            if req_getusers[0]:
                retry_cnt = 0
                if req_getusers[1]:
                    for user in req_getusers[1]:
                        users.append(user)
                        nrofallusers = req_getusers[2]
                    self.signals.fetchedusers.emit(len(users), nrofallusers)
                else:
                    ended = True
            else:
                if req_getusers[1]:
                    if retry_cnt > max_request_retries:
                        return (False,)
                    else:
                        logging.info('Request failed. Number of retries: {}/{}'.format(retry_cnt, max_request_retries))
                        retry_cnt += 1
                else:
                    ended = True
            if retry_cnt <= 0:
                page += 1
        self.signals.fetchedusers.emit(len(users), nrofallusers)
        return (True, users)

    def get_users(self,
                  userstatus='',
                  page=0,
                  pagesize=10,
                  orderby='lastname.sort,firstname.sort',
                  widthadminfields=False):
        """Retrieves users

        :param userstatus: The user status (as string)
        :param page: The page to fetch
        :param pagesize: The page size to fetch
        :param orderby: Order by variables
        :param widthadminfields: Boolean flag whether to fetch with admin fields 
        """
        try:
            url = self._get_get_users_url(page=page,
                                          pagesize=pagesize,
                                          status=userstatus,
                                          orderby=orderby,
                                          widthadminfields=widthadminfields)
            req = Request(url, method='GET')
            for key in self.additionalheaders:
                value = self.additionalheaders[key]
                if value:
                    logging.debug('Setting additional header "{}:{}"'.format(key, value))
                    req.add_header(key, value)
            with urlopen(req) as request:
                decoded = request.read().decode('utf-8')
                jsonobj = json.loads(decoded)
                if jsonobj['content']:
                    return (True, jsonobj['content'], jsonobj['totalElements'] if 'totalElements' in jsonobj else -1)
                else:
                    return (False, False, -1)
        except HTTPError as exception:
            logging.exception('HTTPError')
            return (False, exception, -1)

    def _extractheaders(self, request):
        """Extracts request headers

        :param request: The request
        """
        if self.coyodata.multibackend:
            if self.coyodata.sessionname:
                logging.debug('Extracting cookie header...')
                header_setcookie = request.getheader('Set-Cookie')
                extracted = False
                if header_setcookie:
                    regex = r'' + self.coyodata.sessionname + '=(.*);'
                    matches = re.findall(regex, header_setcookie)
                    if matches:
                        self.additionalheaders['Cookie'] = self.coyodata.sessionname + '=' + matches[0]
                        logging.debug('Extracted additional header "Cookie" with session name "{}"'.format(self.coyodata.sessionname))
                        extracted = True
                if not extracted:
                    logging.debug('Could not extract additional header "Cookie" with session name "{}"'.format(self.coyodata.sessionname))
                    self.additionalheaders['Cookie'] = None
            else:
                self.additionalheaders['Cookie'] = None
                logging.debug('Reset additional header "Cookie"')
                logging.error('Could not extract cookie header. Set "sessionname" in the config file from the correct header "Set-Cookie" from the request:')
                logging.debug(request.info())

    def _get_baseurl(self):
        """Returns the base URL, stripped of backslashes at the end"""
        if self.coyodata.baseurl.endswith('/'):
            self.coyodata.baseurl = self.coyodata.baseurl[:-1]
        return self.coyodata.baseurl

    def _get_gettoken_url(self):
        """Returns the 'get token' URL"""
        #unamep = '&username=' + self.username + '&password=' + self.password
        return self._get_baseurl() + self._urls['token'] + '?' + self._urlparams['granttype'] # + unamep

    def _get_get_users_url(self,
                           page=0,
                           pagesize=10,
                           status='ACTIVE',
                           orderby='lastname.sort,firstname.sort',
                           widthadminfields=False):
        """Returns the 'get users' URL

        :param page: The page to fetch
        :param pagesize: The page size to fetch
        :param status: The user status to fetch
        :param orderby: Order by variables
        :param widthadminfields: Boolean flag whether to fetch with admin fields 
        """
        urlparams = '?' + self._urlparams['acctoken'] + self.accesstoken
        urlparams += '&' + self._urlparams['page'] + str(page)
        urlparams += '&' + self._urlparams['pagesize'] + str(pagesize)
        urlparams += '&' + self._urlparams['status'] + status
        urlparams += '&' + self._urlparams['orderby'] + orderby
        if widthadminfields:
            urlparams += '&' + self._urlparams['withadminfields']
        return self._get_baseurl() + self._urls['users'] + urlparams

    def _retrieve_oauthdata(self):
        """Retrieves the OAuth2 data"""
        logging.debug('Retrieving OAuth2 data...')
        post_fields = {
            'username': self.coyodata.username,
            'password': self.coyodata.password
        }
        base64string = base64.b64encode('{}:{}'
                                        .format(self.coyodata.clientid, self.coyodata.clientsecret).encode('utf-8'))
        base64string_stripped = str(base64string)[2:-1]
        try:
            req = Request(self._get_gettoken_url())
            for key in self.additionalheaders:
                value = self.additionalheaders[key]
                if value:
                    logging.debug('Setting additional header "{}:{}"'.format(key, self.additionalheaders[key]))
                    req.add_header(key, self.additionalheaders[key])
            req.add_header('Authorization', 'Basic {}'.format(base64string_stripped))
            with urlopen(req, bytes(urlencode(post_fields).encode())) as request:
                self._extractheaders(request)
                logging.debug('Successfully retrieved OAuth2 data')
                decoded = request.read().decode('utf-8')
                self.oauthdata = json.loads(decoded)
                return True
        except HTTPError:
            logging.exception('Could not retrieve access token')
            self.valid = False

    def _get_accesstoken(self):
        """Retrieves the access token"""
        self.valid = False
        self.accesstoken = None
        if self._retrieve_oauthdata():
            logging.debug('Extracting access token...')
            try:
                self.accesstoken = self.oauthdata['access_token']
                self.valid = True
                logging.debug('Successfully extracted access token')
            except KeyError:
                logging.exception('Could not extract access token')
        return self.accesstoken
